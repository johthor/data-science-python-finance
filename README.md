# Python for Data Science. And Finance.

This jupyter-notebook can be used to determine average returns of arbitrary stocks and funds. The files in this repository will be used in the Meetup *Data Science mit Python. Und Finanzen.*.

For example, for an exchange traded funds based on the S&P500, the output will look like this for the years between 1983 and 2018: Return-Triangle S&P500:

![Triangle](triangle.png)

# Installation

Please download and install anaconda from https://www.anaconda.com/products/individual. 

Change directory to /data-science-python-finance and execute
```console
$ conda env create -f environment.yml
$ conda activate DataSciencePythonFinance
$ jupyter labextension install @jupyter-widgets/jupyterlab-manage
$ jupyter lab build
```
# Start Notebook Server
```
$ jupyter lab
```